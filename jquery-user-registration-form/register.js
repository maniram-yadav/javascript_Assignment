$(document).ready(function(){
   
DrawCaptcha();
var code="";
 $("#register").click(function(){
       
		resetStyle();
	   var member,email,pass,repassword,fname,mname,lname,gender,mob,display,captcha,accept;
	   
	   var status=true;
	   var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;

	   accept=$("#accept").val();
	   member=$("#registerid").val();
	   email=$("#email").val();
	   pass=$("#pass").val();
	   
	   cpass=$("#cpass").val();
	   fname=$("#fname").val();
	   mname=$("#mname").val();
	   
	   displayname=$("#dname").val();
	   mobile=$("#mob").val();
	   gender=$("#gender").val();
	   
	   captcha=$("#captchval").val();
	   
	if(captcha!=$("#captcha").text()){
		status=false;
		  $("#lblcaptcha").css("color","red");
		  $(".error-captcha").text("Please enter valid captcha");
	}
	
	   if(member==null||member=="")
	  {
		  status=false;
		  $("#lblmember").css("color","red");
		 
	  }
	  if((email==null||email==""||email==" ")&&!regexEmail.test(email))
	  {
		  status=false;
		  $("#lblemail").css("color","red");
		  $(".error-email").text("Please enter valid name(example ***@**.com)");
		  
	  }
	  
	   if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
  {
    console.log("");
  }
  else{
	   $(".error-email").text("Please enter valid name(example ***@**.com)");
  }
	
		   if(pass.length<8)
	  {
		  status=false;
		  $("#lblpass").css("color","red");
		  $(".error-pass").text("Password should be minimum 8 characters.");
	  
	  }
	  if(pass==null||pass==""||pass==" ")
	  {
		  status=false;
		  $("#lblpass").css("color","red");
		  $(".error-pass").text("Please enter the password");
	  
	  }
	  if(pass!=cpass){
		   status=false;
		  $("#lblcpass").css("color","red");
		  $(".error-cpass").text("Password Mismatch");
	  
	  }
	  
	    if(fname==null||fname=="")
	  {
		  status=false;
		  $("#lblfname").css("color","red");
		  $(".error-name").text("Enter valid name");
	  }
	  if(gender==null||gender=="")
	  {
		  status=false;
		  $("#lblgender").css("color","red");
		  $(".error-gender").text("Please select a gender");
	  }
	  if(mobile!=""&&mobile.length!=10)
	  {
		  status=false;
		  $("#lblmob").css("color","red");
		  $(".error-mob").text("Please enter 10 digit mob no");
	  }
	  
	 if($('#accept').is(":checked")==false){
		 $("#cond").css("color","red");
	 }
	 
	 
	 if(status==true){
		 $("body").html("<h3>Registration Successfull.</h3>");
	 }
	   
    });
  
  
 $("#cancel").click(function(){
	   
	  resetStyle();
    });
	
	
	
	function resetStyle(){
		
		  $("#lblmember").css("color","black");
		
		  $("#lblemail").css("color","black");
		  $(".error-email").text("");
	  
	  
		  $("#lblpass").css("color","black");
		  $(".error-pass").text("");
	  
	      $("#lblcpass").css("color","black");
		  $(".error-cpass").text("");
		  
		  $("#lblpass").css("color","black");
		  $(".error-pass").text("");
	 
		  $("#lblfname").css("color","black");
		  $(".error-name").text("");
	
		  $("#lblgender").css("color","black");
		  $(".error-gender").text("");
		  
		  $("#lblmob").css("color","black");
		  $(".error-mob").text("");
		
		 $("#cond").css("color","black");
		  $("#lblcaptcha").css("color","black");
		  $(".error-captcha").text("");
	 
	}
	
	 function DrawCaptcha()
    {
		var length=6
		var a,b,c;
		code="";
		var str="0123456789abcdeFGHIJKLMNOPQfghijklmnopqrstuvwxyzABCDERSTUVWXYZ";
		
		for(var i=0;i<length;i++){
		a = Math.ceil(Math.random() * 10);
        b = Math.ceil(Math.random() * 60); 	
		
		c=a%2;
		
		if(c==0){
			code = code+str.charAt(b%10);
		}
		else{
			code = code+str.charAt(b%61);
		}
		}
		
        $("#captcha").text(code);
    }
});
